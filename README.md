# cloudfront

### How to:
-   add `static('static/', document_root=settings.STATIC_ROOT)` to the url patterns
-   `pip install storages boto3`
-   create cloud front with default settings
-   set up `CLOUDFRONT_DOMAIN` and `AWS_S3_CUSTOM_DOMAIN` to the cloudfront domain
-   set up `CLOUDFRONT_ID`
-   set up `AWS_LOCATION` to where the files will be save in the bucket (probably static)
-   on uploading the files the boto3 will search for credentials. (recommended: if working local set the credentials on the user level)


Needed :

        STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
        DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
        
        AWS_STORAGE_BUCKET_NAME = "cloudfront-test-idrra"
        AWS_DEFAULT_ACL = "public-read"
        
        AWS_HEADERS = {
            'Cache-Control': 'max-age=86400',
        }
        
        CLOUDFRONT_DOMAIN = "d36u7mjgb9fgv7.cloudfront.net"
        CLOUDFRONT_ID = "ELP2YSJUNKTR4"
        AWS_S3_CUSTOM_DOMAIN = 'd36u7mjgb9fgv7.cloudfront.net'
        
        AWS_S3_OBJECT_PARAMETERS = {
            'CacheControl': 'max-age=86400',
        }
        AWS_LOCATION = 'static'
        
        STATIC_URL = f'https://{CLOUDFRONT_DOMAIN}/static/'
        STATIC_ROOT = STATIC_URL
    
